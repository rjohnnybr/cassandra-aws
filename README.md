# Cassandra Cluster AWS com Chef

Script Python para iniciar e configurar completamente um cluster Cassandra no AWS

Importante
Certifique-se de ter os módulos python boto3, json e argparse instalados.
Para instalar o tipo "pip install modulo".

- Edite o arquivo cassandra-qion-cluster.py e adicione as Credenciais da AWS
- Envie o script userdata para um bucket S3
- Altere o path para o bucket S3 que contém o script userdata
- Execute o aplicativo com o comando "python cassandra-qion-cluster.py"
