#!/usr/bin/env python
########################################################################################################
#                                                                                                      #
#  Cassandra cluster Monsanto                                                                          #
#  Arquitetura Cloud Wealthsystems                                                                     #
#                                                                                                      #
########################################################################################################

import boto3
import argparse

##############################################
####           AWS Credentials            ####
##############################################

conn = {
    'aws_access_key_id': '<AWS_ACCESS_KEY>',
    'aws_secret_access_key': '<AWS_SECRET_KEY>',
    'region_name': 'sa-east-1'
}

userdata = "#!/bin/bash \n aws s3 cp s3://cassandra-cluster/config/cassandra-userdata.sh ./ \n bash cassandra-userdata.sh"
key = 'aws_rsa'
securitygroup = 'sg-f1b97597'
instanceprofile = 'arn:aws:iam::876358039431:instance-profile/EC2AccessFull'

parser = argparse.ArgumentParser(description='Argumentos Padroes...')
parser.add_argument('-a', '--ami', action="store", dest="ami", default="ami-f1337e9d", help="Define a AMI a ser usada")
parser.add_argument('-s', '--subnet', action="store", dest="subnet", default="subnet-4193d726", help="Define a subnet a ser usada")
parser.add_argument('-i', '--instancetype', action="store", dest="instancetype", default="c3.xlarge", help="Define o tipo da instancia")

args = parser.parse_args()
ami = args.ami
subnet = args.subnet
instancetype = args.instancetype

##############################################
####           Inicia node 1              ####
##############################################

print "Iniciando Instancia do node cassandra1..."

ec2 = boto3.resource('ec2', **conn)
create_instance = ec2.create_instances(
    ImageId=ami,
    MinCount=1,
    MaxCount=1,
    KeyName=key,
    InstanceType=instancetype,
    SecurityGroupIds=[securitygroup],
    UserData=userdata,
    IamInstanceProfile={
        'Arn': instanceprofile
    },
    SubnetId=subnet
)

instanceid = create_instance[0].id
instance = create_instance[0]
instance.wait_until_running()

# Tag da Instancia
client = boto3.client('ec2', **conn)
client.create_tags(
    Resources = [instanceid],
    Tags= [{"Key": "Name", "Value": "cassandra1"}]
)

instance.load()

print "Id da Instancia %s" % instanceid
print "IP: %s" % instance.public_ip_address
print "Tipo Instancia: %s\n" % instancetype


##############################################
####           Inicia node 2             ####
##############################################

print "Iniciando Instancia do node cassandra2..."

ec2 = boto3.resource('ec2', **conn)
create_instance = ec2.create_instances(
    ImageId=ami,
    MinCount=1,
    MaxCount=1,
    KeyName=key,
    InstanceType=instancetype,
    SecurityGroupIds=[securitygroup],
    UserData=userdata,
    IamInstanceProfile={
        'Arn': instanceprofile
    },
    SubnetId=subnet
)

instanceid = create_instance[0].id
instance = create_instance[0]
instance.wait_until_running()

# Tag da instancia
client = boto3.client('ec2', **conn)
client.create_tags(
    Resources = [instanceid],
    Tags= [{"Key": "Name", "Value": "cassandra2"}]
)

instance.load()

print "Id da Instancia %s" % instanceid
print "IP: %s" % instance.public_ip_address
print "Tipo Instancia: %s\n" % instancetype


##############################################
####           Inicia node 3              ####
##############################################

print "Iniciando Instancia node cassandra3..."

ec2 = boto3.resource('ec2', **conn)
create_instance = ec2.create_instances(
    ImageId=ami,
    MinCount=1,
    MaxCount=1,
    KeyName=key,
    InstanceType=instancetype,
    SecurityGroupIds=[securitygroup],
    UserData=userdata,
    IamInstanceProfile={
        'Arn': instanceprofile
    },
    SubnetId=subnet
)

instanceid = create_instance[0].id
instance = create_instance[0]
instance.wait_until_running()

# Tag instance
client = boto3.client('ec2', **conn)
client.create_tags(
    Resources = [instanceid],
    Tags= [{"Key": "Name", "Value": "cassandra3"}]
)

instance.load()

print "Id da Instancia %s" % instanceid
print "IP: %s" % instance.public_ip_address
print "Tipo Instancia: %s\n" % instancetype
