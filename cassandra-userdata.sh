#!/bin/bash
#####################################################################
# cassandra cluster userdata.sh                                     #
# Arquitetura Cloud Wealthsystems                                   #
#####################################################################

# Pega o data das variaveis
HOST=`aws ec2 describe-tags --region sa-east-1 --filters "Name=resource-id,Values=\`curl -s http://169.254.169.254/latest/meta-data/instance-id\`" "Name=key,Values=Name"  | grep Value | awk -F "\"" '{print $4}'`
DOMAIN="ws.qion.agr.br"
HOSTNAME=$HOST.$DOMAIN
IPADDR=`curl -s http://169.254.169.254/latest/meta-data/local-ipv4`
ZONEID=`aws route53 list-hosted-zones-by-name --dns-name $DOMAIN | grep hostedzone | awk -F / '{print $3}' | sed s/\",//g`

# Altera o hostname dos seeds de acordo com o Name tag setado
echo "preserve_hostname: true" >> /etc/cloud/cloud.cfg
echo "HOSTNAME=$HOSTNAME" >>/etc/sysconfig/network
echo "$HOSTNAME" >/etc/hostname
hostname $HOSTNAME

# time para inicialização da instancias
sleep 180

# seta IPs dos nodes no /etc/hosts
aws ec2 describe-instances --region sa-east-1 --filters 'Name=instance.group-name,Values=cassandra_cluster' --query 'Reservations[].Instances[].[PrivateIpAddress,Tags[?Key==`Name`].Value[]]' --output text | sed '$!N;s/\n/ /' >> /etc/hosts

# Registra a instancia na Route53
    TMPFILE=$(mktemp /tmp/temporary-file.XXXXXXXX)
    cat > ${TMPFILE} << EOF
    {
      "Comment":"Adicionando instancia na route53",
      "Changes":[
        {
          "Action":"UPSERT",
          "ResourceRecordSet":{
            "Name": "$HOSTNAME.",
            "Type": "A",
            "TTL": 300,
            "ResourceRecords":[
              {
                "Value":"$IPADDR"
              }
            ]
          }
        }
      ]
    }
EOF

# Seta na inicialização a zona de DNS no Route 53 e na instncia
aws route53 change-resource-record-sets --hosted-zone-id $ZONEID --change-batch file://"$TMPFILE"
sleep 120 # Aguadando a propagação do DNS
sed -i s/ec2.internal/ws.qion.agr.br/g /etc/resolv.conf
yum install git -y
git -C /tmp clone https://rjohnnybr@bitbucket.org/rjohnnybr/cassandra-aws.git
mv /tmp/cassandra-aws/chef /etc
rpm -Uvh https://packages.chef.io/stable/el/6/chef-12.11.18-1.el6.x86_64.rpm
chef-solo -c /etc/chef/solo.rb

# Seta as config in /etc/cassandra/cassandra.yaml
sed -i -e "s/\(listen_address: \).*/\1$IPADDR/" \
-e "s/\(rpc_address: \).*/\1$IPADDR/" /etc/cassandra/conf/cassandra.yaml
